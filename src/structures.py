'''
Classes for files, folders and collections of them.
'''

import os
import shutil
import sys


class File():
    '''
    Structure representing a file.
    '''
    def __init__(self, path):
        '''
        Initializes the file object.
        path should be absolute and include only forward slashes.
        '''
        self.path = path

    def __eq__(first, second):
        '''
        (Classmethod).
        Checks if the two files have the same content.
        '''
        boolean_output = False

        with open(first.path) as first_file:
            with open(second.path) as second_file:
                boolean_output = first_file.readlines() == second_file.readlines()
        
        return boolean_output

    def exists(self):
        '''
        Checks if this file exists.
        '''
        return os.path.exists(self.path) and not os.path.exists(self.path + "/")

    def get_name(self):
        '''
        Returns the file name.
        '''
        name_start = (self.path.rfind("/") + 1) if "/" in self.path else 0
        return self.path[name_start:]

    def get_extension(self):
        '''
        Returns the file extension.
        '''
        file_name = self.get_name()
        if "." in file_name:
            ext_start = file_name.rfind(".") + 1
            return file_name[ext_start:]
        else:
            return ""

    def move_to(self, dst_folder):
        '''
        Moves this file to the destination folder.
        Returns True if this file has been moved, otherwise False.
        '''
        try:
            path = shutil.move(self.path, dst_folder)
            self.path = path.replace("\\", "/")
        except:
            print("An error occured in moving the file", self.get_name(),
                "to", dst_folder, ":", sys.exc_info()[0], sys.exc_info()[1])
            return False
        else:
            return True


class Folder():
    '''
    Structure representing a directory.
    '''
    def __init__(self, path):
        '''
        Initializes the folder object.
        path should be absolute and include only forward slashes.
        '''
        path = path.replace("\\", "/").rstrip("/")
        path += "/"
        self.path = path

    def exists(self):
        '''
        Checks if this folder exists.
        '''
        return os.path.exists(self.path)

    def get_name(self):
        '''
        Returns the folder name.
        '''
        self.path = self.path.rstrip("/")
        name_start_pos = (self.path.rfind("/") + 1) if "/" in self.path else 0
        return self.path[name_start_pos:]

    def create(self):
        '''
        Creates a new directory with the path of this folder object.
        Returns True if the folder has been created, otherwise False.
        '''
        try:
            os.mkdir(self.path)
        except:
            print("An error occured while creating the folder",
                self.get_name(), ":", sys.exc_info()[0], sys.exc_info()[1])
            return False
        else:
            return True

    def move_to(self, dst_folder):
        '''
        Moves this folder to the destination folder.
        Returns True if this folder has been moved, otherwise False.
        '''
        try:
            path = shutil.move(self.path, dst_folder)
            path = path.rstrip("/")
            path += "/"
            self.path = path.replace("\\", "/")
        except:
            print("An error occured while moving the folder", self.get_name(),
                "to", dst_folder, ":", sys.exc_info()[0], sys.exc_info()[1])
            return False
        else:
            return True

    def get_files(self):
        '''
        Returns a list containing all the files of this directory.
        '''
        if not self.exists():
            print("An error occured while retrieving the files in the folder",
                self.get_name(), ":", sys.exc_info()[0], sys.exc_info()[1])
            return list()
        else:
            members_list = [
                File(self.path + obj) for obj in os.listdir(self.path) if (
                    File(self.path + obj).exists())
            ]
            return members_list

    def delete_folder(self):
        '''
        Deletes this folder and all its contents.
        Returns True if this folder has been deleted, otherwise False.
        '''
        try:
            shutil.rmtree(self.path)
        except:
            print("An error occured while deleting the folder",
                self.get_name(), ":", sys.exc_info()[0], sys.exc_info()[1])
            return False
        else:
            return True


class ObjectList():
    '''
    Structure representing a list of files or folders (or both).
    To be used to perform group actions on its members.
    '''
    def __init__(self):
        '''
        Initializes this file or folder list.
        '''
        self.objects = list()

    def add(self, objects_to_add):
        '''
        Adds the specified objects to this ObjectList.
        objects_to_add must be an iterable.
        '''
        for object_to_add in objects_to_add:
            if object_to_add.exists() and isinstance(
                    object_to_add, (File, Folder)):
                self.objects.append(object_to_add)

    def move_objects_to(self, dst_folder, objects_type="all"):
        '''
        Moves all objects of type objects_type in this ObjectList
        to the destination folder.
        object_type can be any of "file", "folder" and "all".
        Returns True if all objects were moved successfully, otherwise False.
        '''
        output = True
        for each_object in self.objects:
            if objects_type == "all":
                if not each_object.move_to(dst_folder):
                    output = False
            elif isinstance(each_object, File) and objects_type == "file":
                file = each_object
                if not file.move_to(dst_folder):
                    output = False
            elif isinstance(each_object, Folder) and objects_type == "folder":
                folder = each_object
                if not folder.move_to(dst_folder):
                    output = False
            else:
                pass
        return output

    def delete_objects(self, objects_type="all"):
        '''
        Deletes all objects of type objects_type in this ObjectList.
        objects_type can be any of "file", "folder" and "all".
        '''
        folder_to_delete = Folder(self.objects[0].path + "_to_delete")
        count = 0
        while folder_to_delete.exists():
            count += 1
            folder_to_delete.path += str(count)
        if folder_to_delete.create():
            self.move_objects_to(folder_to_delete.path, objects_type)
            folder_to_delete.delete_folder()
        self.remove_stale_refs()

    def remove_stale_refs(self):
        '''
        Removes all references to objects that do not exist
        from this ObjectList.
        '''
        self.objects = [
            each_object for each_object in self.objects if each_object.exists()
        ]
