'''
Main module for the grouper application.
'''

import sys

from structures import Folder, ObjectList


def install(folder_path="."):
    '''
    Transfers the contents of the specified folder (default is the
    current folder) to Python's site-packages folder.
    '''
    src_folder = Folder(folder_path)
    dst_folder = Folder(sys.base_exec_prefix + "/lib/site-packages")
    if src_folder.exists() and dst_folder.exists():
        src_folder_list = ObjectList()
        src_folder_list.add(src_folder.get_files())
        if src_folder_list.move_objects_to(dst_folder.path):
            print("Successfully installed.")
        else:
            print("The installation was not totally successful.")
    else:
        print("The installation was not successful because the folder:",
              (src_folder.path if not src_folder.exists() else dst_folder.path),
              "does not exist."
             )

if __name__ == "__main__":
    install(sys.argv[1])
