* Quick summary: Python package for file and folder manipulation (moving around and deleting), both individually and in groups, using an OOP interface. See the Structures.txt file in the documentation folder for more details.

* Version: 1.1

* Repo owner: bitbucket username - eshiofune

* Set up for use: Navigate to the src/ folder in your terminal, then type in:
                    python install.py

* You can now import structures into your project and make use of it.